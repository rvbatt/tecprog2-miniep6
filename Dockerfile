FROM caddy:2.5.1

COPY Caddyfile /etc/caddy/Caddyfile

WORKDIR /usr/src/pages

COPY pages/index.html ./

COPY pages/about.html ./

